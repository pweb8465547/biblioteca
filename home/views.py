from django.shortcuts import render

from django.views.generic import TemplateView

from autor.models import Autor
from emprestimo.models import Emprestimo
from leitor.models import Leitor
from livro.models import Livro

class IndexView(TemplateView):
    template_name = 'index.html'

    def get_context_data(self, **kwargs):
        context = super(IndexView, self).get_context_data(**kwargs)
        context['qtd_autores'] = Autor.objects.count()
        context['qtd_livros'] = Livro.objects.count()
        context['qtd_leitores'] = Leitor.objects.count()
        context['qtd_emprestimos'] = Emprestimo.objects.count()
        return context