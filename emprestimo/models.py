from django.db import models
from django.db.models.signals import m2m_changed
from django.dispatch import receiver


class Emprestimo(models.Model):
    STATUS_OPCOES = (
        ('A', 'Em Andamento'),
        ('R', 'Realizado'),
    )
    nome = models.CharField('Nome do Empréstimo', max_length=255, blank=True, null=True, help_text='Nome do empréstimo')
    leitor = models.ForeignKey('leitor.Leitor', verbose_name='Leitor', help_text='Leitor que realizou o empréstimo', on_delete=models.CASCADE)
    livros = models.ManyToManyField('livro.Livro', verbose_name='Livros', help_text='Livros emprestados')
    data_emprestimo = models.DateField('Data de Empréstimo', help_text='Data em que os livros foram emprestados')
    data_devolucao = models.DateField('Data de Devolução', help_text='Data prevista para a devolução dos livros')
    status = models.CharField('Status', max_length=15, help_text='Status do empréstimo', choices=STATUS_OPCOES, default=None,)


    class Meta:
        verbose_name = 'Empréstimo'
        verbose_name_plural = 'Empréstimos'
        ordering = ['data_emprestimo', ]

    def __str__(self):
        return f'Leitor: {self.leitor.nome} - Status: {self.get_status_display()}'

    def pre_save(self, *args, **kwargs):
        if self.id is not None:
            original_obj = Emprestimo.objects.get(pk=self.id)
            if self.status != original_obj.status:
                self._atualizar_numero_copias_on_save()

    def save(self, *args, **kwargs):
        self.pre_save()
        super().save(*args, **kwargs)


    def delete(self, *args, **kwargs):
        self._atualizar_numero_copias_on_delete()
        super().delete(*args, **kwargs)

    def _atualizar_numero_copias_on_save(self):
        for livro in self.livros.all():
            if self.status == 'A':
                livro.copias -= 1
            elif self.status == 'R':
                livro.copias += 1
            livro.save()

    def _atualizar_numero_copias_on_delete(self):
        for livro in self.livros.all():
            if self.status == 'A':
                livro.copias += 1
            livro.save()


@receiver(m2m_changed, sender=Emprestimo.livros.through)
def atualizar_copias_livros(sender, instance, **kwargs):
    if kwargs['action'] == 'post_add':
        if instance.status == 'A':
            for livro in instance.livros.all():
                livro.copias -= 1
                livro.save()