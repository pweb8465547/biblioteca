from django.shortcuts import render
from django.db.models import Q
from django.urls import reverse_lazy
from django.views.generic import ListView, CreateView, UpdateView, DeleteView

from .forms import EmprestimoForm
from .models import Emprestimo

class EmprestimosView(ListView):
    model = Emprestimo
    template_name = 'emprestimos.html'

    def get_queryset(self, *args, **kwargs):
        buscar = self.request.GET.get('buscar')
        qs = super(EmprestimosView, self).get_queryset(*args, **kwargs)
        if buscar:
            qs = qs.filter(Q(leitor__nome__icontains=buscar) | Q(livros__titulo__icontains=buscar))
        return qs

class EmprestimoAddView(CreateView):
    form_class = EmprestimoForm
    model = Emprestimo
    template_name = 'emprestimo_form.html'
    success_url = reverse_lazy('emprestimos')

class EmprestimoUpdateView(UpdateView):
    form_class = EmprestimoForm
    model = Emprestimo
    template_name = 'emprestimo_form.html'
    success_url = reverse_lazy('emprestimos')

class EmprestimoDeleteView(DeleteView):
    model = Emprestimo
    template_name = 'emprestimo_apagar.html'
    success_url = reverse_lazy('emprestimos')