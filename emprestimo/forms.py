from django import forms

from .models import Emprestimo

class EmprestimoForm(forms.ModelForm):
    class Meta:
        model = Emprestimo
        fields = ['nome', 'leitor', 'livros', 'data_emprestimo', 'data_devolucao', 'status']

        widgets = {
            'nome': forms.TextInput(attrs={'class': 'input', 'placeholder': 'Digite o nome do empréstimo'}),
            'leitor': forms.Select(attrs={'class': 'input'}),
            'livros': forms.CheckboxSelectMultiple(attrs={'class': 'is-large', 'size': '10'}),
            # 'data_emprestimo': forms.SelectDateWidget(attrs={'class': 'input', 'type': 'date'}),
            # 'data_devolucao': forms.SelectDateWidget(attrs={'class': 'input', 'type': 'date'}),
            'data_emprestimo': forms.DateInput(attrs={'class': 'input', 'type': 'date'}),
            'data_devolucao': forms.DateInput(attrs={'class': 'input', 'type': 'date'}),
            'status': forms.Select(attrs={'class': 'input'}),
        }
