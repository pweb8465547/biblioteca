from django.contrib import admin
from .models import Emprestimo


class LivroEmprestimoInline(admin.TabularInline):
    model = Emprestimo.livros.through
    extra = 1


@admin.register(Emprestimo)
class EmprestimoAdmin(admin.ModelAdmin):
    list_display = ('nome','leitor','livros_emprestados', 'data_emprestimo', 'data_devolucao', 'status')
    list_filter = ('status',)
    filter_horizontal = ('livros',)
    inlines = [LivroEmprestimoInline]

    def get_queryset(self, request):
        return super().get_queryset(request).prefetch_related('livros')

    def livros_emprestados(self, obj):
        return ', '.join([livro.titulo for livro in obj.livros.all()])

    livros_emprestados.short_description = 'Livros Emprestados'