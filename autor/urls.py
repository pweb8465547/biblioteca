from django.urls import path
from .views import AutoresView, AutorAddView, AutorUpDateView, AutorDeleteView

urlpatterns = [
    path('autores/', AutoresView.as_view(), name='autores'),
    path('autor/adicionar/', AutorAddView.as_view(), name='autor_adicionar'),
    path('<int:pk>/autor/editar/', AutorUpDateView.as_view(), name='autor_editar'),
    path('<int:pk>/autor/apagar/', AutorDeleteView.as_view(), name='autor_apagar'),
]
