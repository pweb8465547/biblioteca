from django import forms
from .models import Autor

class AutorForm(forms.ModelForm):
    class Meta:
        model = Autor
        fields = ['nome', 'ano_nascimento']

        widgets = {
            'nome': forms.TextInput(attrs={'class': 'input', 'placeholder': 'Digite o nome do autor'}),
            # 'ano_nascimento': forms.SelectDateWidget(attrs={'class': 'input', 'type': 'date'}),
            'ano_nascimento': forms.DateInput(attrs={'class': 'input', 'type': 'date'}),
        }
