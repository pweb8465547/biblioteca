from django.db import models

class Autor(models.Model):
    nome = models.CharField('Nome', max_length=50, help_text='Nome do autor')
    ano_nascimento = models.DateField('Ano de Nascimento', help_text='Ano de nascimento do autor')

    class Meta:
        verbose_name = 'Autor'
        verbose_name_plural = 'Autores'
        ordering = ['nome', ]

    def __str__(self):
        return self.nome
