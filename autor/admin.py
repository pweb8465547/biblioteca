from django.contrib import admin
from .models import Autor

@admin.register(Autor)
class AutorAdmin(admin.ModelAdmin):
    list_display = ('nome', 'ano_nascimento')
    search_fields = ('nome', 'ano_nascimento')
