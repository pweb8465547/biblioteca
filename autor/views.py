from django.shortcuts import render
from django.db.models import Q
from django.urls import reverse_lazy
from django.views.generic import ListView, CreateView, UpdateView, DeleteView

from .forms import AutorForm
from .models import Autor


class AutoresView(ListView):
    model = Autor
    template_name = 'autores.html'

    def get_queryset(self, *args, **kwargs):
        buscar = self.request.GET.get('buscar')
        qs = super(AutoresView, self).get_queryset(*args, **kwargs)
        if buscar:
            qs = qs.filter(Q(nome__icontains=buscar) | Q(ano__icontains=buscar))
        return qs


class AutorAddView(CreateView):
    form_class = AutorForm
    model = Autor
    template_name = 'autor_form.html'
    success_url = reverse_lazy('autores')


class AutorUpDateView(UpdateView):
    form_class = AutorForm
    model = Autor
    template_name = 'autor_form.html'
    success_url = reverse_lazy('autores')


class AutorDeleteView(DeleteView):
    model = Autor
    template_name = 'autor_apagar.html'
    success_url = reverse_lazy('autores')
