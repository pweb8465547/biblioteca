from django.contrib import admin
from .models import Leitor

@admin.register(Leitor)
class LeitorAdmin(admin.ModelAdmin):
    list_display = ('nome',)
    search_fields = ('nome',)
