from django.db import models


class Leitor(models.Model):
    nome = models.CharField('Nome', max_length=50, help_text='Nome do leitor')

    class Meta:
        verbose_name = 'Leitor'
        verbose_name_plural = 'Leitores'
        ordering = ['nome', ]

    def __str__(self):
        return self.nome
