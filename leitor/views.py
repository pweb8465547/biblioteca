from django.shortcuts import render
from django.db.models import Q
from django.urls import reverse_lazy
from django.views.generic import ListView, CreateView, UpdateView, DeleteView

from .forms import LeitorModelForm
from .models import Leitor

class LeitoresView(ListView):
    model = Leitor
    template_name = 'leitores.html'

    def get_queryset(self, *args, **kwargs):
        buscar = self.request.GET.get('buscar')
        qs = super(LeitoresView, self).get_queryset(*args, **kwargs)
        if buscar:
            qs = qs.filter(Q(nome__icontains=buscar))
        return qs


class LeitorAddView(CreateView):
    form_class = LeitorModelForm
    model = Leitor
    template_name = 'leitor_form.html'
    success_url = reverse_lazy('leitores')


class LeitorUpDateView(UpdateView):
    form_class = LeitorModelForm
    model = Leitor
    template_name = 'leitor_form.html'
    success_url = reverse_lazy('leitores')


class LeitorDeleteView(DeleteView):
    model = Leitor
    template_name = 'leitor_apagar.html'
    success_url = reverse_lazy('leitores')