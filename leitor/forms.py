from django import forms
from .models import Leitor

class LeitorModelForm(forms.ModelForm):
    class Meta:
        model = Leitor
        fields = ['nome']
        widgets = {
            'nome': forms.TextInput(attrs={'class': 'input', 'type': 'text', 'placeholder': 'Digite o nome do leitor'}),
        }
