from django.urls import path
from .views import LeitoresView, LeitorAddView, LeitorUpDateView, LeitorDeleteView

urlpatterns = [
    path('leitores/', LeitoresView.as_view(), name='leitores'),
    path('leitor/adicionar/', LeitorAddView.as_view(), name='leitor_adicionar'),
    path('<int:pk>/leitor/editar/', LeitorUpDateView.as_view(), name='leitor_editar'),
    path('<int:pk>/leitor/apagar/', LeitorDeleteView.as_view(), name='leitor_apagar'),
]

