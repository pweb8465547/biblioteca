from django.db import models
from autor.models import Autor

class Livro(models.Model):
    titulo = models.CharField('Título', max_length=50, help_text='Título do livro')
    autor = models.ForeignKey(Autor, on_delete=models.CASCADE, help_text='Autor do livro')
    genero = models.CharField('Gênero', max_length=50, help_text='Gênero do livro')
    copias = models.DecimalField('Cópias', max_digits=5, decimal_places=0, help_text='Número de cópias disponíveis')

    class Meta:
        verbose_name = 'Livro'
        verbose_name_plural = 'Livros'
        ordering = ['titulo', ]

    def __str__(self):
        return self.titulo
