from django.shortcuts import render
from django.db.models import Q
from django.urls import reverse_lazy
from django.views.generic import ListView, CreateView, UpdateView, DeleteView

from .forms import LivroForm
from .models import Livro


class LivrosView(ListView):
    model = Livro
    template_name = 'livros.html'

    def get_queryset(self, *args, **kwargs):
        buscar = self.request.GET.get('buscar')
        qs = super(LivrosView, self).get_queryset(*args, **kwargs)
        if buscar:
            qs = qs.filter(Q(titulo__icontains=buscar) | Q(genero__icontains=buscar))
        return qs


class LivroAddView(CreateView):
    form_class = LivroForm
    model = Livro
    template_name = 'livro_form.html'
    success_url = reverse_lazy('livros')


class LivroUpDateView(UpdateView):
    form_class = LivroForm
    model = Livro
    template_name = 'livro_form.html'
    success_url = reverse_lazy('livros')


class LivroDeleteView(DeleteView):
    model = Livro
    template_name = 'livro_apagar.html'
    success_url = reverse_lazy('livros')
