from django.contrib import admin
from .models import Livro

@admin.register(Livro)
class LivroAdmin(admin.ModelAdmin):
    list_display = ('titulo', 'autor', 'genero', 'copias')
    search_fields = ('titulo', 'autor__nome')
    list_filter = ('genero', 'autor')
