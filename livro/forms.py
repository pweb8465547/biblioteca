from django import forms

from autor.models import Autor
from .models import Livro

class LivroForm(forms.ModelForm):
    class Meta:
        model = Livro
        fields = ['titulo', 'autor', 'genero', 'copias']

        widgets = {
            'titulo': forms.TextInput(attrs={'class': 'input', 'placeholder': 'Digite o título do livro'}),
            # 'autor': forms.ModelChoiceField(queryset=Autor.objects.all(), empty_label="Selecione um autor"),
            'genero': forms.TextInput(attrs={'class': 'input', 'placeholder': 'Digite o gênero do livro'}),
            'copias': forms.NumberInput(attrs={'class': 'input', 'placeholder': 'Digite o número de cópias'}),
        }
